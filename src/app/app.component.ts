import { Component, OnInit, AfterViewInit } from '@angular/core';
import { EventService } from './services/api/event.service';
import { Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { DeviceDetectorService } from 'ngx-device-detector';
import { SocketService } from './services/socket/socket.service';
import { DataService } from './services/data.service';
import { DatePipe } from '@angular/common';
import { HistoryService } from './services/api/history.service';


declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  public subDomain: string;
  public isBoard: boolean = false;
  public isIe = false;
  public messageIndex = 0;

  constructor(
    private router: Router,
    private eventService: EventService,
    private historyService: HistoryService,
    private deviceService: DeviceDetectorService,
    private socketService: SocketService,
    private dataService: DataService,
    private datePipe: DatePipe,
  ) {
    this.router.events
      .pipe(filter((e): e is NavigationEnd => e instanceof NavigationEnd))
      .subscribe((event) => {
        this.isBoard = (event.url === '/board' || event.url === '/board-regist');
        // * 페이지 이동시 열려있는 모달 모두 닫기
        $('.modal').modal('hide');
      });

    // 소켓 초기화
    this.socketService.init();
  }

  ngOnInit(): void {
    const device = this.deviceService.getDeviceInfo();
    if (device.browser === 'IE') {
      this.isIe = true;
      this.router.navigate(['/not-ie']);
    }

    // 토스트 데이터 수신 구독
    const date = this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:mm');
    this.dataService.currentReceiveMessage
      .subscribe((data) => {
        if (!data) {
          return;
        }

        if (data.roomId) {
          const room = this.historyService.getRoom();
          if (!room) {
            return;
          }
          if (room.id !== data.roomId) {
            return;
          }
        }

        const innerHTML = `
      <div class="toast-header">
      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-info-circle-fill" viewBox="0 0 16 16">
      <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412l-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
      </svg>
        <strong class="mr-auto">&nbsp;Notification</strong>
        <small>${date}</small>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="toast-body">
      <b>${data.title}</b><br>
        ${data.description}
      </div>`;

        const toastElement = document.createElement('div');
        toastElement.setAttribute('id', `notiToast-${this.messageIndex}`);
        toastElement.setAttribute('role', 'alert');
        toastElement.setAttribute('aria-live', 'assertive');
        toastElement.setAttribute('aria-atomic', 'true');
        toastElement.setAttribute('class', 'toast');
        toastElement.setAttribute('data-autohide', 'false');
        toastElement.innerHTML = innerHTML;
        $('.toast-wrapper').append(toastElement);
        $(`#notiToast-${this.messageIndex}`).toast('show');
        this.messageIndex++;
      });
  }

  ngAfterViewInit(): void {
  }

  openKakao(): void {
    window.open('http://pf.kakao.com/_xgJEyK/chat', '_blank');
  }
}
