import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import * as io from 'socket.io-client';
import { Router } from '@angular/router';
import { HistoryService } from '../api/history.service';
import { MemberService } from '../api/member.service';
import { DataService } from '../data.service';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private url = 'https://cfair-socket-ssl.cf-air.com'; // 운영
  // private url = 'http://192.168.1.11:38038'; // 로컬서버가 띄운 소켓서버

  private socket;
  private user;
  private currentRoomId;
  private memberId_live;
  public receiveMessage = new BehaviorSubject(false);

  constructor(
    public router: Router,
    private historyService: HistoryService,
    private memberService: MemberService,
    private dataService: DataService,
  ) {
  }

  init(): void {
    this.socket = io(this.url, {
      transports: ['websocket']
    });

    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    // 소켓 연결
    this.socket.on('connect', () => {
      console.log('socket server on.');
      if (this.user) {
        this.login({
          memberId: this.user.id,
          token: this.user.token
        });
      }
      if (this.currentRoomId) {
        this.join(this.currentRoomId);
      }
      // console.log('connected');
    });

    // 소켓 연결 해제
    this.socket.on('disconnect', () => {
      console.log('disconnected');
    });

    this.socket.on('message', (data) => {
      // console.log('수신', data);
      const user = JSON.parse(sessionStorage.getItem('cfair'));
      if (user && user.eventId && user.eventId === data.eventId) {
        this.dataService.changeReceiveMessage(data);
      }
    });

    // (행사종료 시 관리자->서버->사용자 소켓전송) 메인화면으로 강제 이동 (시청 중일 경우 out남기고 이동)
    this.socket.on('forceLogout', (data) => {
      console.log(data);
      const user = JSON.parse(sessionStorage.getItem('cfair'));
      if (user && user.eventId && user.eventId === data.eventId) {
        if (!this.historyService.isAttended()) {
          const options: { relationId: string, relationType: string, logType: string } = {
            relationId: sessionStorage.getItem('currentRoom'),
            relationType: 'room',
            logType: this.historyService.getAttendance()
          };
          // 서버에 저장
          this.memberService.history(
            this.memberId_live,
            options).subscribe(res => {
              sessionStorage.removeItem('currentRoom');
              this.historyService.setAttendance(null);

              alert('금일 행사가 종료되었습니다.');
              this.router.navigate(['/']);
            });
        } else {
          this.router.navigate(['/']);
        }
      }
    });

    // multiViewing
    this.socket.on('multiViewing', (data) => {
      console.log('multiView');
      this.router.navigate(['/']);
    });


    // 중복 로그인
    this.socket.on('new_login', (data) => {
      // console.log('new_login', data);
      this.logout({
        memberId: data.memberId
      });

      /**
       * 라이브에서 중복로그인 발생 시
       * 튕기기 전에 history-out 전송해야 하기때문에 memberId_live에 임시로 저장한다.
       */
      this.memberId_live = data.memberId;
      if (!this.historyService.isAttended()) {
        const options: { relationId: string, relationType: string, logType: string } = {
          relationId: sessionStorage.getItem('currentRoom'),
          relationType: 'room',
          logType: this.historyService.getAttendance()
        };
        // 서버에 저장
        this.memberService.history(
          this.memberId_live,
          options).subscribe(res => {
            sessionStorage.removeItem('currentRoom');
          });

        this.historyService.setAttendance(null);
      }

      // out 후에 토큰 remove
      sessionStorage.removeItem('cfair');

      // alert('다른 기기에서 로그인하여\n자동으로 로그아웃 되었습니다.');
      alert('Sign in from another device.');
      this.router.navigate(['/']);
    });
  }

  getUser(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
  }

  login(data): void {
    this.socket.emit('login', data, (result) => {
      // console.log(result);
    });
  }

  /**
   * 로그아웃 전달
   */
  logout(data): void {
    this.socket.emit('logout', data, (result) => {
      // console.log(result);
      this.currentRoomId = null;
    });
  }

  /**
   * Room 입장
   * @param roomId 룸 ID
   */
  join(roomId): void {
    this.getUser();

    this.socket.emit('join', {
      id: roomId,
      type: 'room',
      memberId: this.user.id
    }, callback => {
      console.log('in', callback);
    })

    this.currentRoomId = roomId;
  }

  /**
   * Room 퇴장
   * @param roomId 룸 ID
   */
  leave(roomId): void {
    this.getUser();
    // console.log(this.memberId_live);

    // live에서 퇴장 시 user정보가 소실되었기 때문에 임시로 저장해 둔 memberId_live 이용해서 소켓에 leave를 알려준다.
    this.socket.emit('leave', {
      id: roomId,
      type: 'room',
      memberId: this.memberId_live ? this.memberId_live : this.user.id,
    });

    // 값 비워주기
    this.memberId_live = null;
    this.currentRoomId = null;
  }

}
