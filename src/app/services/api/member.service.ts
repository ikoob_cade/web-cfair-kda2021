import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class MemberService {
  private serverUrl = '/events/:eventId/members';

  constructor(private http: HttpClient) { }

  /**
   * 사용자 강의별 시청기록 조회
   * @param memberId 사용자 일련번호
   */
  getMyHistory(memberId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + memberId + '/history')
      .pipe(catchError(this.handleError));
  }

  /**
   * 사용자 세션별 시청기록 조회
   * @param memberId 사용자 일련번호
   */
   getMyHistoryOfSession(memberId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + memberId + '/historyOfSession')
      .pipe(catchError(this.handleError));
  }

  /**
   * 사용자 VOD 시청기록 조회
   * @param memberId 사용자 일련번호
   * @param startDate 행사 시작일
   * @param endDate 행사 종료일
   */
  getMyHistoryOfVod(memberId: string, startDate: string, endDate: string): Observable<any> {
    const params = {
      params: {
        startDate,
        endDate
      }
    };

    return this.http.get(this.serverUrl + '/' + memberId + '/historyOfVod', params)
      .pipe(catchError(this.handleError));
  }

  /**
   * 프로그램 In and Out 히스토리 생성
   * @param memberId 사용자 일련번호
   * @param body 룸 일련번호, 입/출 타입
   */
  history(memberId: string, body: any): Observable<any> {
    return this.http.post(this.serverUrl + '/' + memberId + '/history', body)
      .pipe(catchError(this.handleError));
  }

  historyOfVod(memberId: string, body: any): Observable<any> {
    return this.http.post(this.serverUrl + '/' + memberId + '/historyOfVod', body)
      .pipe(catchError(this.handleError));
  }

  /**
   * 사용자 출석 생성
   * @param memberId 사용자 일련번호
   */
  attendance(memberId: string): Observable<any> {
    return this.http.post(this.serverUrl + '/' + memberId + '/attendance', null)
      .pipe(catchError(this.handleError));
  }

  /**
   * 댓글 조회
   * @param memberId 사용자 일련번호
   * @param relationId 룸/아젠다/트랙 ID
   */
  findComment(memberId: string, relationId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + memberId + '/comment/' + relationId)
      .pipe(catchError(this.handleError));
  }

  /**
   * 댓글 작성
   * @param memberId 사용자 일련번호
   * @param title? 댓글 제목
   * @param content 댓글 내용
   * @param relationId 룸/아젠다/트랙 ID
   */
  createComment(params: { memberId: string, title?: string, content: string, relationId: string }): Observable<any> {
    return this.http.post(this.serverUrl + '/' + params.memberId + '/comment',
      {
        relationId: params.relationId,
        content: params.content
      })
      .pipe(catchError(this.handleError));
  }

  /**
   * 댓글 삭제
   * @param memberId 사용자 일련번호
   * @param commentId 댓글 ID
   */
  deleteComment(memberId: string, commentId: string): Observable<any> {
    return this.http.delete(this.serverUrl + '/' + memberId + '/comment/' + commentId)
      .pipe(catchError(this.handleError));
  }

  /**
   * 개인정보 조회
   * @param memberId 사용자 일련번호
   */
  getMyInfo(memberId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + memberId)
      .pipe(catchError(this.handleError));
  }

  /**
   * 비밀번호 변경
   * @param memberId 사용자 일련번호
   * @param password 새 비밀번호
   */
  changePassword(memberId: string, password: string): Observable<any> {
    return this.http.put(this.serverUrl + '/' + memberId + '/changePassword', { password });
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

  /**
   * 방문 조회
   * @param memberId 사용자 일련번호
   */
  findVisitors(memberId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + memberId + '/visitor')
      .pipe(catchError(this.handleError));
  }

}
