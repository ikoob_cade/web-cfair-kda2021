import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class SpeakerService {
  private serverUrl = '/events/:eventId/speakers';

  constructor(private http: HttpClient) { }

  /** 발표자 리스트 조회
   * @param isSpeakerPage Speakers 페이지
   */
  find(isSpeakerPage?: boolean): Observable<any> {
    const params = {
      params: {
        sort: 'fullName',
        asc: '1',
        role: '0',
      }
    };
    if (isSpeakerPage) {
      params.params.role = '1';
      // params.params.sort = 'seq';
    }

    return this.http.get(this.serverUrl, params)
      .pipe(catchError(this.handleError));
  }

  findOne(speakerId: string): Observable<any> {
    return this.http.get(this.serverUrl + '/' + speakerId)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }

    // Return an observable with a user-facing error message.
    return throwError('Something bad happened; please try again later.');
  }

}
