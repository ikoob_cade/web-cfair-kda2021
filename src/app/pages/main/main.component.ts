import * as moment from 'moment';
import * as _ from 'lodash';
import { _ParseAST } from '@angular/compiler';
import { Component, OnInit, AfterViewInit, HostListener, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { SpeakerService } from '../../services/api/speaker.service';
import { BoothService } from '../../services/api/booth.service';
import { CookieService } from 'ngx-cookie-service';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('entryAlertBtn') entryAlert: ElementRef;
  public speakers: Array<any> = [];
  public sponsors: Array<any> = []; // 스폰서 목록
  public selectedBooth: any;
  public mobile: boolean;

  public programBookUrl = '';
  public excerptUrl = '';

  public user;
  private koreaTimer;
  public koreaTime;

  public attachments = []; // 부스 첨부파일

  public booths: Array<any> = []; // 카테고리[부스] 목록

  constructor(
    private speakerService: SpeakerService,
    private boothService: BoothService,
    private cookieService: CookieService,
  ) { }


  @HostListener('window:resize', ['$event'])
  onResize(event): void {
    if (window.innerWidth < 768 !== this.mobile) {
      this.loadSpeakers();
    }
  }

  /**
   * 배열 나누기
   * @param array 배열
   * @param n n개씩 자르기
   */
  division = (array, n) => {
    return [].concat.apply([],
      array.map((item, i) => {
        return i % n ? [] : [array.slice(i, i + n)]
      })
    );
  }

  ngOnInit(): void {
    this.user = sessionStorage.getItem('cfair');
    this.loadSpeakers();
    this.getTime();
    this.loadBooths();
  }

  ngAfterViewInit(): void {
    // if (this.cookieService.get(`${environment.eventId}_main_popup`)) {
    //   let cookieTime = this.cookieService.get(`${environment.eventId}_main_popup`);
    //   const now = new Date().getTime().toString();
    //   if (cookieTime < now) {
    //     this.cookieService.delete(`${environment.eventId}_main_popup`);
    //     this.policyAlertBtn.nativeElement.click();
    //   }
    // } else {
    // setTimeout(() => {
    //   this.entryAlert.nativeElement.click();
    // }, 0);
  }

  ngOnDestroy(): void {
    clearInterval(this.koreaTimer);
  }


  // 발표자 리스트를 조회한다.
  loadSpeakers(): void {
    let limit = 6;
    this.mobile = window.innerWidth < 768;
    if (this.mobile) { limit = 6; }
    this.speakerService.find(false).subscribe(res => {
      this.speakers = this.division(res, limit);
    });
  }

  getTime(): void {
    this.koreaTimer = setInterval(() => {
      this.koreaTime = moment().utc().utcOffset('+0900'); // 한국 시간 : UTC +0900
    }, 1000);
  }

  loadBooths = () => {
    this.boothService.find().subscribe(res => {
      this.booths = res.slice(0, 14);

      // const categories =
      //   _.chain(res)
      //     .groupBy(booth => {
      //       return booth.category ? JSON.stringify(booth.category) : '{}';
      //     })
      //     .map((booth, category) => {
      //       category = JSON.parse(category);
      //       category.booths = booth;

      //       // console.log('category= ', category)
      //       return category;
      //     }).sortBy(category => {
      //       return category.seq;
      //     })
      //     .value();

      // this.booths = categories;
    });
  }

}
