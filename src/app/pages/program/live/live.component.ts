import * as _ from 'lodash';
import { UAParser } from 'ua-parser-js';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy, HostListener, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart, RouterEvent, NavigationCancel } from '@angular/router';

import { forkJoin as observableForkJoin, Observable } from 'rxjs';
import { AgendaService } from '../../../services/api/agenda.service';
import { DateService } from '../../../services/api/date.service';
import { RoomService } from '../../../services/api/room.service';
import { map } from 'rxjs/operators';
import { MemberService } from '../../../services/api/member.service';
import { HistoryService } from '../../../services/api/history.service';
import { SocketService } from '../../../services/socket/socket.service';
import { BannerService } from '../../../services/api/banner.service';
import { NgImageSliderComponent } from 'ng-image-slider';
import { DomSanitizer } from '@angular/platform-browser';
import { StarRatingComponent } from 'ng-starrating';
import { RateService } from '../../../services/api/rate.service';
declare var $: any;
import * as moment from 'moment-timezone';

@Component({
  selector: 'app-live',
  templateUrl: './live.component.html',
  styleUrls: ['./live.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LiveComponent implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('entryAlertBtn') entryAlertBtn: ElementRef;
  @ViewChild('bannerSlider') bannerSlider: NgImageSliderComponent;
  @ViewChild('starRate') starRate: ElementRef;
  public user: any; // 사용자 정보
  public live: any; // 라이브 방송
  public isLiveAgenda: any; // 현재 진행중인 아젠다
  public liveAgendaState: any;

  public attendance: boolean; // 출석상태

  public dates: any[] = []; // 날짜 목록
  public rooms: any[] = []; // 룸 목록

  public agendas: any[] = []; // 날짜/룸으로 필터링 된 강의.

  public liveChatUrl;

  public selected: any = {
    date: null,
    room: null
  };

  public showPlayer = false;

  public banners = []; // 광고배너 리스트
  public bannersLiveChat = []; // 플레이어에 넘겨줄 채팅창 위 롤링배너 데이타
  public classColMd = false;

  public exitButton = false;

  private exitRouteUrl = null;
  private routerSubscription;
  constructor(
    public router: Router,
    public route: ActivatedRoute,
    private dateService: DateService,
    private roomService: RoomService,
    private agendaService: AgendaService,
    private memberService: MemberService,
    private historyService: HistoryService,
    private socketService: SocketService,
    private bannerService: BannerService,
    private rateService: RateService,
    private sanitizer: DomSanitizer,
  ) {
    this.doInit();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (window.innerWidth < 1070) {
      this.classColMd = true;
    } else {
      this.classColMd = false;
    }
  }

  ngOnInit(): void {
    this.user = JSON.parse(sessionStorage.getItem('cfair'));
    this.getBanners();

    if (window.innerWidth < 1070) {
      this.classColMd = true;
    } else {
      this.classColMd = false;
    }
  }

  ngAfterViewInit(): void {
    this.routerSubscription = this.router.events.subscribe((ev: RouterEvent) => {
      if (ev instanceof NavigationCancel && !ev.reason) {
        this.exitRouteUrl = ev.url;
        if (this.exitButton) {
          this.entryAlertBtn.nativeElement.click();
        } else {
          this.useEntryPopup = false;
          this.atndnCheck(false, false, false, true);
        }
      }
    });
  }

  doInit(): void {
    this.historyService.setAttendance('in');
    const observables = [this.getDates(), this.getRooms()];
    observableForkJoin(observables)
      .pipe(
        map(resp => {
          this.dates = resp[0];
          this.rooms = resp[1];
          return resp;
        })
      )
      .subscribe(res => {
        let date = this.dates[0];
        let room = this.rooms[0];
        this.route.queryParams.subscribe(param => {
          // 선택된 날짜/룸이 있다면 해당 날짜와 룸이 선택될 수 있도록 한다.
          if (param.dateId && param.roomId) {
            date = this.dates.find(date => { return date.id === param.dateId; });
            room = this.rooms.find(room => { return room.id === param.roomId; });
          }
          else if (this.user.roomId && this.user.dateId) {
            date = this.dates.find(date => { return date.id === this.user.dateId; });
            room = this.rooms.find(room => { return room.id === this.user.roomId; });
          }
          else if (!this.user.roomId && !this.user.dateId) {
            date = this.dates.find(date => {
              return date.id === '607e3504ba4e29001459d9fc';
            }); // 7일
            room = this.rooms.find(room => {
              return room.id === '607e3d9dba4e29001459d9ff';
            }); // Channel A
          }
          this.router.navigate([], { queryParams: { dateId: null, roomId: null }, queryParamsHandling: 'merge' });
        });
        this.selected = { date: date, room: room };
        this.setAgendaList(date, room);
      });
  }

  public next: any;
  // Date/Room 선택 시 Agenda Filtering
  setAgendaList(date: any, room: any, next?: boolean): any {
    this.historyService.setRoom(room);
    // 초기화
    this.exitRouteUrl = null;
    this.useEntryPopup = false;
    /*
    * 날짜의 옵션값 확인해서 player컴포넌트 출력을 제어한다.
    * 선택한 날짜의 ON/OFF 상태를 확인해서 플레이어, 채팅 이용 불가하도록 하기 위함
    */
    if (!date.showPlayer) {
      this.showPlayer = false;
    } else {
      this.showPlayer = true;
    }

    setTimeout(() => {
      this.socketService.leave(this.selected.room.id);
      this.socketService.join(room.id);
    }, this.user ? 0 : 300);

    this.user.dateId = date.id;
    this.user.roomId = room.id;

    sessionStorage.setItem('cfair', JSON.stringify(this.user));

    if (this.live && !this.historyService.isAttended()) {
      this.openEntryAlert();
      if (!this.showPlayer) {
        this.showPlayer = !this.showPlayer;
      }

      this.next = () => {
        this.setAgendaList(date, room, true);
      };
    } else {
      if (next) {
        this.next = null;
      }

      setTimeout(() => {
        this.selected = {
          date,
          room
        };
        this.getAgendasV2(date.id, room.id)
          .subscribe(agendas => {
            this.agendas = agendas;
            setTimeout(() => {
              this.live = this.selected.room.contents;

              // 별도의 채팅창 포함 여부 확인
              if (this.live && this.live.chatIncluded && this.live.chatUrl) {
                this.liveChatUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.live.chatUrl);
              }

              // Live가 있으면 IN / OUT
              if (this.live) {
                if (this.showPlayer) {
                  this.openEntryAlert();
                }
                this.checkLiveAgenda();
              }
              this.rateIndex = 0;
              clearTimeout(this.popupTimer);
              this.initRate();
              this.useEntryPopup = true;
            }, 100);
          });
        this.live = null; // new app-player
      }, 500);
    }
  }

  /**
   * 입장/퇴장 모달을 출력한다.
   * user의 isLog데이터 구분.
   * 해외연자는 연수평점의 의무가 없어서 입장/퇴장이 불필요하다.
   */
  openEntryAlert(): void {
    // 로그 저장하지 않는 경우
    if (!this.user.isLog) {
      return;
    } else {
      // 퇴장 팝업은 생략하고 서버로 전송한다.
      if (this.getAttendance() === 'out') {
        if (this.exitButton) {
          this.entryAlertBtn.nativeElement.click();
        } else {
          this.atndnCheck();
        }
      } else {
        this.entryAlertBtn.nativeElement.click();
      }
    }
  }

  // 현재 진행중인 아젠다 확인
  checkLiveAgenda(): void {
    this.isLiveAgenda = this.agendas[0];
    this.agendas.forEach((agenda: any) => {
      const agendaDate = agenda.date.date.replace(/-/gi, '/');
      const agendaStartTime = new Date(`${agendaDate}/${agenda.startTime}`);
      const agendaEndTime = new Date(`${agendaDate}/${agenda.endTime}`);
      const now: Date = new Date();
      if (now > agendaStartTime && now < agendaEndTime) {
        this.isLiveAgenda = agenda;
      }
    });
  }

  /* 아젠다 목록 조회 V2
   * date, room 값을 파라미터에 추가했음.
   * 위 조건에 해당하는 아젠다만 받아온다 => 서버 부하 감소
   */
  getAgendasV2(dateId, roomId): Observable<any> {
    return this.agendaService.findV2(dateId, roomId);
  }

  /** 날짜 목록 조회 */
  getDates(): Observable<any> {
    return this.dateService.find();
  }

  /** 룸 목록 조회 */
  getRooms(): Observable<any> {
    return this.roomService.find();
  }

  /**
   * 강의 상세보기
   * @param agenda 강의
   */
  goDetail(agenda: any): void {
    this.router.navigate([`/live/${this.selected.date.id}/${this.selected.room.id}/${agenda.id}`]);
  }

  // 출석체크
  atndnCheck(isMove?, isPopup?, isOut?, isExit?): void {
    if (!this.user.isLog) {
      // 로그 저장 안함
      return;
    }

    const options: { relationId: string, relationType: string, logType: string } = {
      relationId: this.selected.room.id,
      relationType: 'room',
      logType: this.historyService.getAttendance()
    };

    sessionStorage.setItem('currentRoom', this.selected.room.id);
    this.memberService.history(this.user.id, options)
      .subscribe(
        (resp) => {
          if (this.historyService.isAttended()) {
            this.historyService.setAttendance('out');
          } else {
            this.historyService.setAttendance('in');
          }

          if (this.next) {
            this.next();
          }

          if (isMove) {
            this.openEntryAlert();
          }

          if (isPopup) {
            this.openEntryAlert();
          } else if (this.useEntryPopup) {
            // this.initAgendaPopup();
          }

          // 퇴장버튼인 경우
          if (isOut) {
            if (this.exitRouteUrl) {
              this.router.navigate([this.exitRouteUrl]);
            } else {
              setTimeout(() => {
                this.entryAlertBtn.nativeElement.click();
              }, 1000);
            }
          }

          if (isExit) {
            this.router.navigate([this.exitRouteUrl]);
          }
        },
        (error) => {
          console.error(error);
        });
  }

  // 참여 상태 출력
  getAttendance(): string {
    return this.historyService.isAttended() ? 'in' : 'out';
  }


  // 슬라이드 배너 신경외과용이니 나중에 복사한다.
  /** 배너목록을 조회한다. */
  getBanners(): void {
    this.bannerService.find().subscribe(res => {
      if (res.live) {
        res.live.forEach(item => {
          const data = {
            link: item.link,
            thumbImage: item.photoUrl,
            alt: item.title,
          };
          this.banners.push(data);
        });
      }
      // const bannersLiveChat = [];
      // res.liveChat.forEach(item => {
      //   const data = {
      //     link: item.link,
      //     thumbImage: item.photoUrl,
      //     alt: item.title,
      //   };
      //   bannersLiveChat.push(data);
      // });

      // if (bannersLiveChat.length > 0) {
      //   this.bannersLiveChat = bannersLiveChat;
      // }
    });
  }

  // 광고 구좌 왼쪽 버튼 클릭
  slidePrev(target): void {
    this[target].prev();
  }
  // 광고 구좌 오른쪽 버튼 클릭
  slideNext(target): void {
    this[target].next();
  }
  // 광고 배너 클릭
  imageClick(index): void {
    if (this.banners[index] && this.banners[index].link) {
      window.open(this.banners[index].link);
    }
  }

  public nextRateAgenda: any = null;
  public nowRating = false;
  private rateIndex = 0;
  public rateAgenda = {};
  public rateTimeout;
  public rateTimer;
  public rateTimeString;

  // 별점 기능 초기화
  initRate(): void {
    // 초기화
    clearTimeout(this.rateTimeout);
    this.nowRating = false;
    this.nextRateAgenda = null;
    this.rateAgenda = {};

    const rateableAgendas = _.filter(this.agendas, agenda => agenda.isRateable);
    // const now = new Date(2021, 2, 31, 15, 19, 55);
    const now = new Date(); //현재 시간

    let rateableList = [];
    for (const agenda of rateableAgendas) {
      const agendaEndTime = agenda.isSession ? agenda.title.split('-')[1] : agenda.endTime;
      const agendaDatetime = `${agenda.date.date} ${agendaEndTime}`;

      // 강의 종료 시간
      const time = new Date(agendaDatetime);
      // 강의 투표종료시간(강의종료시간+=10)이 현재보다 미래인 경우
      if (now.getTime() <= (time.getTime() + (1000 * 60 * 10))) {
        const startTime = new Date(time); // 평점 시작 시간
        const endTime = new Date(time);
        endTime.setMinutes(time.getMinutes() + 10); // 평점 종료시간(10분 후)

        const data = {
          isSession: agenda.isSession,
          agendaId: agenda.id,
          startTime,
          endTime,
          title: agenda.isSession ? agenda.subTitle : agenda.title,
        };

        rateableList.push(data);
      }
    }

    if (rateableList.length > 0 && rateableList[this.rateIndex]) {
      rateableList = _.sortBy(rateableList, 'endTime');
      this.nextRateAgenda = rateableList[this.rateIndex];
      this.rateIndex++;

      // 현재시간과 평점투표 시작시간의 차
      const diffToStart = now.getTime() - this.nextRateAgenda.startTime.getTime();

      // 현재시간과 평점투표 종료시간의 차
      const diffToEnd = now.getTime() - this.nextRateAgenda.endTime.getTime();

      const user = JSON.parse(sessionStorage.getItem('cfair'));

      // 투표 시작시간 전
      if (user && diffToStart < 0) {
        this.rateTimeout = setTimeout(() => {
          // ! 해당 세션에 별점이력이 있는지 확인 true: 별점POST 가능, false: 해당 강의에 별점 이력 존재
          this.rateService
            .findOne(this.nextRateAgenda.agendaId, { memberId: user.id, isSession: this.nextRateAgenda.isSession })
            .subscribe(res => {
              if (res) {
                this.rateAgenda = {
                  title: this.nextRateAgenda.title
                };

                this.rateTimer = setInterval(() => {
                  let nowTime = new Date().getTime();
                  let newTime = new Date(this.nextRateAgenda.endTime.getTime() - nowTime);
                  if (newTime.getMinutes() === 0 && newTime.getSeconds() === 0) {
                    clearInterval(this.rateTimer);
                    this.initRate();
                  } else {
                    this.rateTimeString = newTime;
                  }
                }, 1000);
                this.nowRating = true;
              } else {
                // next
                this.initRate();
              }
            });
          // 이력이 없으면 활성화
        }, Math.abs(diffToStart));
      } else if (user && diffToEnd < 0) {
        // 투표시작,종료시간 이내
        this.rateService
          .findOne(this.nextRateAgenda.agendaId, { memberId: user.id, isSession: this.nextRateAgenda.isSession })
          .subscribe(res => {
            if (res) {
              this.rateAgenda = {
                title: this.nextRateAgenda.title
              };

              this.rateTimer = setInterval(() => {
                let nowTime = new Date().getTime();
                let newTime = new Date(this.nextRateAgenda.endTime.getTime() - nowTime);

                if (newTime.getMinutes() === 0 && newTime.getSeconds() === 0) {
                  clearInterval(this.rateTimer);
                  this.initRate();
                } else {
                  this.rateTimeString = newTime;
                }
              }, 1000);
              this.nowRating = true;
            } else {
              // next
              this.initRate();
            }
          });
      }
    }
  }

  public currentScore = 0;

  /** 평점 */
  onRate($event: { oldValue: number, newValue: number, starRating: StarRatingComponent }): void {
    // alert(`Old Value: ${ $event.oldValue },
    //   New Value: ${$event.newValue},
    //   Checked Color: ${$event.starRating.checkedcolor},
    //   Unchecked Color: ${$event.starRating.uncheckedcolor}`);
    this.currentScore = $event.newValue;
  }

  sendRating(): void {
    let parser = new UAParser();
    let fullUserAgent = parser.getResult();

    let body: any = {
      agendaId: this.nextRateAgenda.agendaId,
      memberId: this.user.id,
      isSession: this.nextRateAgenda.isSession,
      score: this.currentScore,
      userAgent: JSON.stringify(fullUserAgent),
      browser: JSON.stringify(fullUserAgent.browser),
      device: JSON.stringify(fullUserAgent.device),
      engine: JSON.stringify(fullUserAgent.engine),
      os: JSON.stringify(fullUserAgent.os),
      ua: JSON.stringify(fullUserAgent.ua),
    };

    this.rateService.create(body).subscribe(res => {
      alert('Completed');
      clearTimeout(this.rateTimeout);
      clearInterval(this.rateTimer);
      this.rateTimeString = null;
      this.initRate();
    });
  }

  /**
   * 페이지 나갈 때 처리
   */
  ngOnDestroy(): void {
    this.socketService.leave(this.selected.room.id);
    clearTimeout(this.rateTimeout);
    clearInterval(this.rateTimer);
    clearTimeout(this.popupTimer);
    this.historyService.setRoom(null);

    this.routerSubscription.unsubscribe();
  }

  popupTimer;
  useEntryPopup = false;
  /**
   * Agenda 리스트 이용, 각 Agenda의 입장,퇴장 팝업 여부에 따라 해당 시간에 팝업노출한다.
   * 입장상태에서 입장팝업 활성화된 Agenda의 시작시간 도달 시 퇴장 전송, 입장팝업 출력한다.
   *
   */
  initAgendaPopup(): void {
    this.exitButton = false;
    // 타임아웃 초기화
    clearTimeout(this.popupTimer);

    const now = moment().tz('Asia/Seoul');

    const startTimes: any = _.map(this.agendas, agenda => {
      const agendaStartTime = agenda.isSession ? agenda.title.split('-')[0] : agenda.startTime;
      const agendaEndTime = agenda.isSession ? agenda.title.split('-')[1] : agenda.endTime;

      const startDate = moment(`${agenda.date.date} ${agendaStartTime}`).tz('Asia/Seoul'); // 세션시작 Date
      const endDate = moment(`${agenda.date.date} ${agendaEndTime}`).tz('Asia/Seoul'); // 세션종료 Date
      return {
        start: startDate.toDate(),
        end: endDate.toDate(),
        isSession: agenda.isSession,
        startPopup: agenda.startPopup,
        endPopup: agenda.endPopup
      };
    }).filter(agendaTime => { // 현재 시간보다 세션종료가 미래인 경우로 필터링
      return (now < agendaTime.end) && agendaTime.isSession;
    });

    if (!startTimes.length) {
      return;
    }

    // TODO isAttended >> in === true | out === false
    // 시작시간, 종료시간 내에 있을 경우.
    // 행사 진행 중 라이브 페이지 입장 시 동작
    if (startTimes[0].start <= now && now < startTimes[0].end) {
      const popupTime = startTimes[0].end.valueOf() - moment().tz('Asia/Seoul').valueOf(); // 팝업할 종료시간.

      // 퇴장 버튼이 필요한 경우
      if (startTimes[0].endPopup) {
        this.exitButton = true;
      }

      this.popupTimer = setTimeout(() => {
        if (startTimes[0].endPopup) { // TODO 퇴장팝업 해야할 경우
          this.entryAlertBtn.nativeElement.click();
        } else {
          this.atndnCheck(false, true, false);
        }
      }, popupTime);
    } else {
      // 세션 시작시간 전 일경우
      // 일정 중 비어있는 시간에 들어왔을 때 || 행사 시작 전에 접속
      if (now < startTimes[0].start) {
        // 입장상태 체크
        if (this.historyService.isAttended()) { // 입장상태가 아닐때.
          if (this.useEntryPopup) {
            this.entryAlertBtn.nativeElement.click();
          } else {
            this.useEntryPopup = true;
          }
        } else {
          // 입장상태 일때. (행사 시작 전에 페이지 진입해서 입장버튼 누른 상태)
          const popupTime = startTimes[0].start.valueOf() - moment().tz('Asia/Seoul').valueOf(); // 팝업할 시작시간.
          this.popupTimer = setTimeout(() => {
            if (!this.historyService.isAttended()) { // 입장상태 일때
              // 퇴장 전송 후 입장 팝업...
              this.atndnCheck(false, true);

            }
          }, popupTime);
        }
      }
    }
  }

  /**
   * 이 페이지 벗어나기 전에(window:beforeunload) history('out')을 위한 이벤트 리스너
   * @param $event 이벤트
   */
  @HostListener('window:beforeunload', ['$event'])
  public beforeunloadHandler($event) {
    // 로그 저장하는 경우
    if (!this.historyService.isAttended() && this.user.isLog) {
      this.atndnCheck(true);
    }

    $event.preventDefault();
    return false;
  }
}
