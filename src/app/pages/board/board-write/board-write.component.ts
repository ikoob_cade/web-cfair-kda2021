import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BoardService } from '../../../services/api/board.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-board-write',
  templateUrl: './board-write.component.html',
  styleUrls: ['./board-write.component.scss'],
  encapsulation:ViewEncapsulation.None,
})
export class BoardWriteComponent implements OnInit {

  public user: any;
  public formGroup: FormGroup;
  public board: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private boardService: BoardService
  ) { }

  ngOnInit(): void {
    this.setFormGroup();
    if (this.route.snapshot.queryParams.boardId) {
      let boardId = this.route.snapshot.queryParams.boardId;
      this.getBoardById(boardId);
    }
  }

  getBoardById(boardId: string) {
    this.boardService.findOne(boardId)
      .subscribe((resp: any) => {
        this.board = resp;
        this.formGroup.patchValue({
          title: resp.title,
          content: resp.content,
          type: resp.type,
          isFixed: resp.isFixed,
          writer: resp.writer,
          phone: resp.phone,
          email: resp.email,
          password: resp.password,
        });
      });
  }

  setFormGroup(): void {
    this.formGroup = this.fb.group({
      title: ['', Validators.compose([Validators.required])],
      content: ['', Validators.compose([Validators.required])],
      type: ['post', Validators.compose([Validators.required])],
      isFixed: [false],
      writer: ['', Validators.compose([Validators.required])],
      phone: [''],
      email: ['', Validators.compose([Validators.pattern('[a-zA-Z0-9.-_-]{1,}@[a-zA-Z.-]{2,}[.]{1}[a-zA-Z]{2,}'), Validators.required])],
      // password: ['', Validators.compose([Validators.minLength(2)])],
    });

    // 유저 정보기 있을 경우
    if (sessionStorage.getItem('cfair')) {
      this.user = JSON.parse(sessionStorage.getItem('cfair'));
      this.formGroup.patchValue({
        writer: this.user.name,
        email: this.user.email
      });
    }
  }

  // 글 작성 완료
  submit(): void {
    if (this.formGroup.invalid) {
      let target: string;
      if (this.formGroup.controls.content.invalid) {
        target = 'Contents';
      }
      if (this.formGroup.controls.title.invalid) {
        target = 'Title';
      }
      if (this.formGroup.controls.writer.invalid) {
        target = 'Name';
      }
      if (this.formGroup.controls.email.invalid) {
        target = 'Email';
      }
      return alert(` Input your ${target}, please.`);
    } else {
      (this.board && this.board.id) ? this.update() : this.create();
    }
  }


  // 글 추가하기
  create() {
    this.boardService.create(this.formGroup.value)
      .subscribe((resp: any) => {
        this.router.navigate(['/board']);
      });
  }

  // 글 수정하기
  update() {
    this.boardService.update(this.board.id, this.formGroup.value)
      .subscribe((resp: any) => {
        this.router.navigate(['/board']);
      });
  }

  // 글 작성 취소
  cancelWrite() {
    let result = confirm('Are you sure you wish to cancel?');
    if (result) {
      this.router.navigate(['/bloard'])
    }
  }

}
