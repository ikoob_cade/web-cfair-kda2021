import * as _ from 'lodash';
import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BoothService } from '../../services/api/booth.service';
import { MemberService } from '../../services/api/member.service';

declare var $: any;
@Component({
  selector: 'app-e-booth',
  templateUrl: './e-booth.component.html',
  styleUrls: ['./e-booth.component.scss']
})
export class EBoothComponent implements OnInit, AfterViewInit {
  @ViewChild('winAlertBtn') winAlertBtn: ElementRef;
  @ViewChild('notiAlertBtn') notiAlertBtn: ElementRef;

  public sponsors: Array<any> = []; // 스폰서 목록
  private categories: Array<any> = [];
  public categories1: Array<any> = []; // 카테고리[부스] 목록
  public categories2: Array<any> = []; // 카테고리[부스] 목록
  public user: any = JSON.parse(sessionStorage.getItem('cfair'));
  public selectedBooth = null;
  public boothsOfStamp: Array<any>[];
  public attachments = [];
  public random = 0;

  public isLoaded = false;

  private stampTourAccess = true;

  constructor(
    private boothService: BoothService,
    private memberService: MemberService
  ) {
  }

  ngOnInit(): void {
    // this.attachments = this.setAttachments();
    $('#e-boothModal').on('hidden.bs.modal', () => {
      this.selectedBooth = null;
      document.getElementById('boothModalDesc').innerHTML = null;
    });

    this.loadBooths();
    this.getVisitBooth();
  }

  ngAfterViewInit(): void {
  }

  /** 모달에 첨부파일 셋팅 */
  setAttachments(): void {
    if (this.selectedBooth) {
      this.attachments = _.map(this.selectedBooth.contents, (content) => {
        if (content.contentType === 'slide') {
          return content;
        }
      });
    }
  }

  /**
   * 부스 목록 조회
   *
   * atweek2020 작업중 작성
   * 카드형식의 업체도 스탬프투어가 적용되어야할경우 부스관리에 등록 후 배열을 직접 잘라서(slice) html에 배치.
   */
  loadBooths = () => {
    this.boothService.find()
      .subscribe(res => {
        const booths = res.slice(0, 20);
        if (res.length < 1) {
          return;
        }
        this.categories =
          _.chain(booths)
            .groupBy(booth => {
              return booth.category ? JSON.stringify(booth.category) : '{}';
            })
            .map((booth, category) => {
              category = JSON.parse(category);
              category.booths = booth;
              return category;
            }).sortBy(category => {
              return category.seq;
            })
            .value();

        this.categories = _.sortBy(this.categories, 'seq');
        _.forEach(this.categories, category => {
          category.categoryName = category.categoryName.toLowerCase();
        });

        this.categories1 = this.categories;
        this.categories2 = res.slice(15, 29);
        this.isLoaded = true;
      });
  }

  /**
   * 스폰서 목록 조회
   */
  // loadSponsors = () => {
  //   this.sponsorService.find().subscribe(res => {
  //     this.sponsors = res;
  //   });
  // }

  // 모달의 부스내용을 선택한 아이템 데이터로 변경
  setDesc(): void {
    if (this.selectedBooth.description) {
      document.getElementById('boothModalDesc').innerHTML = this.selectedBooth.description;
    }
  }

  /*
   * 부스 조회 (나의 부스방문 기록)
   */
  private getVisitBooth(): void {
    this.memberService.findVisitors(this.user.id)
      .subscribe((data: any) => {
        this.boothsOfStamp = _.filter(data, booth => {
          return booth.category.categoryName !== 'Card';
        });
      });
  }

  // 부스 선택
  getDetail = (selectedBooth) => {
    let memberId = null;

    if (this.user) {
      memberId = this.user.id;
    }

    this.boothService
      .findOne(selectedBooth.id, memberId, this.stampTourAccess)
      .subscribe(res => {
        this.selectedBooth = res;
        this.random = Math.floor(Math.random() * 10);
        if (res.tourSuccess) {
          this.winAlertBtn.nativeElement.click();
        }

        this.setAttachments();
        this.setDesc();
      });
  }

  // 부스 선택
  getDetailV2 = (selectedBoothId) => {
    let memberId = null;
    if (this.user) {
      memberId = this.user.id;
    }

    this.boothService
      .findOne(selectedBoothId, memberId, this.stampTourAccess)
      .subscribe(res => {
        this.selectedBooth = res;
        this.random = Math.floor(Math.random() * 10);
        if (res.tourSuccess) {
          this.winAlertBtn.nativeElement.click();
        }

        this.setAttachments();
        this.setDesc();

        this.getVisitBooth();
      });
  }

}
